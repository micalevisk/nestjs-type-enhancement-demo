import { Module, Inject, forwardRef } from '@nestjs/common';

class FooService {}

@Module({
  providers: [FooService]
})
export class AppModule {
  @Inject() private readonly validInjectUsage: FooService

  constructor(
    @Inject(forwardRef(() => FooService)) private valid: FooService
    ,
    @Inject(undefined) private valid2: FooService
    ,
    @Inject('') private valid3: FooService
    ,
    @Inject() private invalid: FooService
  ) {}
}
